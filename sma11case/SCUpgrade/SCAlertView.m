//
//  SCAlertView.m
//  GBA4iOS
//
//  Created by sma11case on 11/07/2018.
//  Copyright © 2018 Riley Testut. All rights reserved.
//

#import "SCAlertView.h"

#if SC_Enable_Upgrade
#undef SCAlertView
#undef SCActionSheet
#undef SCPopoverController

@implementation SCAlertView
@end

@implementation SCActionSheet
@end

@implementation SCPopoverController
@end
#endif

