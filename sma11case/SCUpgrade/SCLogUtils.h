//
//  SCLogUtils.h
//  GBA4iOS
//
//  Created by sma11case on 11/07/2018.
//  Copyright © 2018 Riley Testut. All rights reserved.
//

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

// remove all logs
#ifndef DEBUG
#undef NSLog
#define NSLog(...)

//#undef printf
//#define printf(...)
#endif

