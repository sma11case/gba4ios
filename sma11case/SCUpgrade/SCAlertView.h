//
//  SCAlertView.h
//  GBA4iOS
//
//  Created by sma11case on 11/07/2018.
//  Copyright © 2018 Riley Testut. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
@interface SCAlertView : UIAlertView
@end

@interface SCActionSheet : UIActionSheet
@end

@interface SCPopoverController : UIPopoverController
@end
#pragma clang diagnostic pop

#define SC_Enable_Upgrade 1UL
#define UIAlertView         SCAlertView
#define UIActionSheet       SCActionSheet
#define UIPopoverController SCPopoverController

#ifndef DEBUG
#undef  SC_Enable_Upgrade
#define SC_Enable_Upgrade 0UL

#undef  UIAlertView
#define SCAlertView UIAlertView

#undef  UIActionSheet
#define SCActionSheet UIActionSheet

#undef  UIPopoverController
#define SCPopoverController UIPopoverController
#endif
