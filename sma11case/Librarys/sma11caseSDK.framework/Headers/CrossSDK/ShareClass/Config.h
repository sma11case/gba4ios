//
//  Config.h
//  sma11case
//
//  Created by sma11case on 15/8/11.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TargetConditionals.h>

#define HaveSma11caseSDK 1UL

#ifdef DEBUG
#undef IS_DEBUG_MODE
#define IS_DEBUG_MODE 1UL
#else
#undef IS_DEBUG_MODE
#define IS_DEBUG_MODE 0UL
#endif

// defined version code
#define BUILD_SafeVersion  1UL // only generic modules, no 3rd library
#define BUILD_StoreVersion 2UL // BUILD_SafeVersion + Safe 3rd librarys
#define BUILD_FullVersion  3UL // BUILD_StoreVersion + Private APIs

// set build version(enable/disable APIs)
#ifndef SCLibVersion
#define SCLibVersion BUILD_FullVersion // set dest build version
#endif

// import custom configs
#if (__has_include("../../SCSDKPrefix.h"))
#import "../../SCSDKPrefix.h"
#endif

// define system constants
#ifndef RedefineSystemConstants
#define RedefineSystemConstants 0UL
#endif

// remove console log on release
#if (!defined(DisableConsoleLog) && !defined(DEBUG))
#define DisableConsoleLog 1UL
#endif

// use enhanceed log ==> tid:1 xxxxxxxxxxxxx
#ifndef UseEnhanceLog
#define UseEnhanceLog 1UL
#endif

#if (DisableConsoleLog)
#undef  NSLog
#define NSLog(...)

#undef  printf
#define printf(...)
#endif

// fix platform macros
#ifndef PLAT_IOS
#if (TARGET_OS_IPHONE && !TARGET_OS_MAC)
#define PLAT_IOS 1UL
#endif
#endif

#ifndef PLAT_OSX
#if (TARGET_OS_MAC && !TARGET_OS_IPHONE)
#define PLAT_OSX 1UL
#endif
#endif

#ifndef PLAT_WATCH
#define PLAT_WATCH 0UL
#endif

#ifndef TARGET_OS_IPHONE
#define TARGET_OS_IPHONE PLAT_IOS
#endif

#ifndef TARGET_OS_IOS
#define TARGET_OS_IOS PLAT_IOS
#endif

#ifndef TARGET_OS_WATCH
#define TARGET_OS_WATCH PLAT_WATCH
#endif

