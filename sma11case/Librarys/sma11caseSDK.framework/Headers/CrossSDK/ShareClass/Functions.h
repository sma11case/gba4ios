//
//  Functions.h
//  sma11case
//
//  Created by sma11case on 15/8/14.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "Macros.h"

typedef void(*pfSendMessage0)(id target, SEL selector, ...);
extern pfSendMessage0 sc_msgSend0;
#define sc_msgSend0(...) sc_msgSend0(__VA_ARGS__)

typedef id(*pfSendMessage1)(id target, SEL selector, ...);
extern pfSendMessage1 sc_msgSend1;
#define sc_msgSend1(...) sc_msgSend1(__VA_ARGS__)

typedef long(*pfSendMessageX)(id target, SEL selector, ...);
extern pfSendMessageX sc_msgSendX;
#define sc_msgSendX(...) sc_msgSendX(__VA_ARGS__)

double randomNumber(double min, double max, BOOL isFloat);
#define RandomNumber(min, max) (size_t)randomNumber(min, max, NO)

unsigned long hexStringToUnsignedLongValue(const char *string);

void *getMethodAddress(id object, SEL selector);

typedef BOOL(^SCConditionBlock)(NSTimeInterval passTime, NSUInteger idx, BOOL *stop);
void runBlockOnCondition(NSTimeInterval interval, NSTimeInterval timeout, SCConditionBlock condition, EmptyBlock block);

void syncBlockWithGroup(dispatch_queue_t queue, dispatch_block_t block, ...);
#define syncBlockWithGroup(...) syncBlockWithGroup(__VA_ARGS__, nil)

typedef void(^MoniterFolderBlock)(NSString *path, dispatch_queue_t queue);
dispatch_source_t moniterFolder(NSString *path, dispatch_queue_t queue, MoniterFolderBlock block1, dispatch_block_t block2);

NSString *getPinyin(NSString * string,BOOL isShort);

char *makeRandomString(char *keys, size_t length);
char *makeCharacterWithCount(NSUInteger count, char character);

NSMutableArray      *getMethodListWithClass(Class cls);
NSMutableDictionary *getPropertyListWithClass(Class cls);
NSMutableDictionary *getIvarListWithClass(Class cls);

// get assign property, __unsafe_unretained object type
BOOL getIvarValue(id instance, const char *name, size_t size, void *buffer);

// set assign property, __unsafe_unretained object type
BOOL setIvarValue(id instance, const char *name, size_t size, void *buffer);

#if IS_DEBUG_MODE
#define GetObjectIvar(x, y) getIvarObject(x, #y); do{ typeof(y) __getIvarObject = nil; __getIvarObject = nil; } while (false)
#else
#define GetObjectIvar(x, y) getIvarObject(x, #y)
#endif
id getIvarObject(id instance, const char *name);

#define GetAssignIvar(x, y) getIvarNumber(x, sizeof(typeof(y)), #y)
double getIvarNumber(id instance, size_t size, const char *name);

typedef void(^SetIvarBlock)(id sender, const char *ivar_name, void *ivar_ptr);
BOOL setIvarAssinEx(id instance, const char *name, SetIvarBlock block);
#define SetIvarAssinEx(x, y, z) setIvarAssinEx(x, #y, z)

#if IS_DEBUG_MODE
#define SetObjectIvar(x, y, z, t) do{ setIvarObject(x, #y, z, t);  __unsafe_unretained typeof(y) __setIvarObject = nil; __setIvarObject = nil; }while(false)
#else
#define SetObjectIvar(x, y, z, t) setIvarObject(x, #y, z, t)
#endif
BOOL setIvarObject(id instance, const char *name, id value, objc_AssociationPolicy policy);

#define CopyAssignIvar(x, y, z) typeof(y) z = GetAssignIvar(x, y);
#define CopyObjectIvar(x, y, z) typeof(y) z = GetObjectIvar(x, y);

extern id sc_superMutableCopy(id object);

size_t getFileSize(const char *file);
NSStringEncoding getFileEncodingFromBOM(const char *file);
NSString *getCommonRootPath(NSString *path1, NSString *path2);

NSDictionary *getITunesAppInfo(NSString *appid);

NSString *sc_crc32(void *ptr, size_t len);
NSString *sc_md5(void *ptr, size_t len);
NSString *sc_sha1(void *ptr, size_t len);

