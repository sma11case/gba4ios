//
//  SDKConfig.h
//  sma11case
//
//  Created by sma11case on 02/11/2017.
//
//

#import <Foundation/Foundation.h>
#import "Macros.h"

typedef void(*pfMLog)(NSString *s1, ...);
typedef void(*pfCLog)(const char *s1, ...);

extern double kSCLibVersion;
#if IS_DEBUG_MODE
#define kSCLibVersion ^{return kSCLibVersion;}()
#endif

extern pfMLog ptr_sc_mlog;
#define sc_mlog(...) do{ if (ptr_sc_mlog) ptr_sc_mlog(__VA_ARGS__); }while(false)

extern pfCLog ptr_sc_clog;
#define sc_clog(...) do{ if (ptr_sc_clog) ptr_sc_clog(__VA_ARGS__); }while(false)



