//
//  NSThread+SC.h
//  sma11case
//
//  Created by lianlian on 8/30/16.
//
//

#import <Foundation/Foundation.h>
#import "../Config.h"
#import "../SDKConfig.h"

#ifndef PrintFunctionName
#define PrintFunctionName() do{\
NSString *call = [[NSThread callStackSymbols][0] substringFromIndex:40];\
printf("%s\n", call.UTF8String);\
}while(0)
#endif

#ifndef PrintFunctionNameEnhance
#define PrintFunctionNameEnhance() do{\
NSString *call = [[NSThread callStackSymbols][0] substringFromIndex:40];\
NSString *temp = [NSString stringWithFormat:@"tid:%zd %@", [NSThread currentThread].seqNumber, call];\
printf("%s\n", temp.UTF8String);\
}while(0)
#endif

#ifndef LogFunctionName
#define LogFunctionName()

#if (IS_DEBUG_MODE && !UseEnhanceLog)
#undef LogFunctionName
#define LogFunctionName() PrintFunctionName()
#endif

#if (IS_DEBUG_MODE && UseEnhanceLog)
#undef LogFunctionName
#define LogFunctionName() PrintFunctionNameEnhance()
#endif
#endif

#if DisableConsoleLog
#undef  sc_printf
#define sc_printf(...)

#undef  sc_NSLog
#define sc_NSLog(...)
#endif

#if (UseEnhanceLog && !DisableConsoleLog)
#undef  printf
#define printf(...) sc_printf(__VA_ARGS__)

#undef  NSLog
#define NSLog(...)  sc_NSLog(__VA_ARGS__)
#endif

extern int  sc_printf(const char *c1, ...);
extern void sc_NSLog(NSString *c1, ...);

extern void sc_UseSystemLogFunction();
extern void sc_UseEnhanceLogFunction();

@interface NSThread(sma11case_shareClass)
- (NSInteger)seqNumber;
@end


