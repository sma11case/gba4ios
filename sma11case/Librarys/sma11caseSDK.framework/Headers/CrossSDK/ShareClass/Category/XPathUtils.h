//
//  XPathUtils.h
//  LianAi
//
//  Created by sma11case on 02/04/2018.
//  Copyright © 2018 Yung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Macros.h"

extern void sc_xpathSetDelimiter(NSString *sep);

typedef id(^SCXPathObjectBlock)(NSString *xpath);
typedef NSArray *(^SCXPathArrayBlock)(NSString *xpath);
typedef NSDictionary *(^SCXPathDictionaryBlock)(NSString *xpath);
typedef NSString *(^SCXPathStringBlock)(NSString *xpath);
typedef NSNumber *(^SCXPathNumberBlock)(NSString *xpath);

@interface NSArray(sma11case_XPath)
@property (nonatomic, strong, readonly) SCXPathArrayBlock xArray;
@property (nonatomic, strong, readonly) SCXPathNumberBlock xNumber;
@property (nonatomic, strong, readonly) SCXPathDictionaryBlock xDictionary;
@property (nonatomic, strong, readonly) SCXPathStringBlock xString;
@property (nonatomic, strong, readonly) SCXPathObjectBlock xObject;

- (void)setXPath: (NSString *)xpath value: (id)object;
@end

@interface NSDictionary(sma11case_XPath)
@property (nonatomic, strong, readonly) SCXPathArrayBlock xArray;
@property (nonatomic, strong, readonly) SCXPathNumberBlock xNumber;
@property (nonatomic, strong, readonly) SCXPathDictionaryBlock xDictionary;
@property (nonatomic, strong, readonly) SCXPathStringBlock xString;
@property (nonatomic, strong, readonly) SCXPathObjectBlock xObject;

- (void)setXPath: (NSString *)xpath value: (id)object;
@end

//@interface NSArray(sma11case_XPath)
//- (void)setXPath: (NSString *)xpath value: (id)object;
//@end
//
//@interface NSMutableDictionary(sma11case_XPath)
//- (void)setXPath: (NSString *)xpath value: (id)object;
//@end

