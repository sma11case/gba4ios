//
//  NSMutableDictionary+SC.h
//  sma11case
//
//  Created by sma11case on 07/03/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (sma11case_ShareClass)
- (void)setWeakObject:(id)anObject forKey:(id<NSCopying>)aKey;
@end
