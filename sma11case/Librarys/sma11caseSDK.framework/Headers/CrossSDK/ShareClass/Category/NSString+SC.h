//
//  NSString+Crypt.h
//  sma11case
//
//  Created by sma11case on 15/4/20.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../CrossPlatform/CrossPlatform.h"
#import "../typedef.h"

#if PLAT_IOS
#define CPFont UIFont
#endif

#if PLAT_OSX
#define CPFont NSFont
#endif

extern NSString *sc_mstr(NSString *first, ...);
#define sc_mstr(...) sc_mstr(__VA_ARGS__, nil)

@interface NSString (sma11case_ShareClass)
+ (NSString *)randomStringWithKeys: (char *)keys length: (size_t)length;

- (NSString *)crc32;
- (NSString *)md5;
- (NSString *)sha1;

- (id)toJSONObject;
- (NSData *)toUTF8Data;
+ (unsigned long)toUnsignedLongValue: (NSString *)string;

- (NSArray *)rangesOfString:(NSString *)searchString searchRange: (NSRange)searchRange;

- (NSData *)base64Decode;

- (CGSize)calcSizeWithFont: (CPFont *)font width: (CGFloat)width height: (CGFloat)height;
@end
