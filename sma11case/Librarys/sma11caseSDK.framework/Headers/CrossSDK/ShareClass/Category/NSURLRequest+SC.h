//
//  NSURLRequest+SC.h
//  sma11case
//
//  Created by sma11case on 14/10/2017.
//

#import <Foundation/Foundation.h>
#import "../Config.h"

typedef NSString *(^URLEncodeBlock)(id object);

extern NSMutableString *generateCurl(NSString *url, NSDictionary *headers, id postData, URLEncodeBlock block);

@interface NSURLRequest(sma11case_ShareClass)
- (NSMutableString *)generateCurlWithURLEncodeBlock: (URLEncodeBlock)block;
@end
