//
//  NSMutableSet+SC.h
//  sma11case
//
//  Created by sma11case on 07/03/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableSet (sma11case_ShareClass)
- (void)addWeakObject:(id)object;
@end
