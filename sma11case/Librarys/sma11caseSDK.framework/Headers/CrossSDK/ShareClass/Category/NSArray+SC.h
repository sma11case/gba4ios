//
//  NSArray+SC.h
//  sma11case
//
//  Created by sma11case on 11/5/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray(sma11case_ShareClass)
- (NSData *)toData: (NSInteger)type; // 1=PlistFormat 2=JsonFormat
- (NSString *)toJsonString;
- (id)weakObjectAtIndex:(NSUInteger)index;
@end

