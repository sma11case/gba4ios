//
//  NSSet+SC.h
//  sma11case
//
//  Created by sma11case on 07/03/2017.
//
//

#import <Foundation/Foundation.h>

@interface NSSet (sma11case_ShareClass)
- (id)anyWeakObject;
@end
