//
//  NSValue+SC.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/19.
//

#import <Foundation/Foundation.h>

@interface NSValue(sma11case_shareClass)
@property (nonatomic, assign, readonly) SEL selectorValue;

+ (NSValue *)valueWithSelector:(SEL)selector;
@end
