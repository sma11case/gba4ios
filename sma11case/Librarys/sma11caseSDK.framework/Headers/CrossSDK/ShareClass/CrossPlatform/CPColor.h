//
//  CPColor.h
//  sma11case
//
//  Created by sma11case on 11/23/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Config.h"

#if PLAT_IOS
#import <UIKit/UIKit.h>
#define CPColor UIColor
#endif

#if PLAT_OSX
#import <Cocoa/Cocoa.h>
#define CPColor NSColor
#endif

// 颜色宏
#define kClearColor [CPColor clearColor]
#define kWhiteColor [CPColor whiteColor]
#define kBlackColor [CPColor blackColor]
#define kRedColor   [CPColor redColor]
#define kBlueColor  [CPColor blueColor]
#define kGreenColor [CPColor greenColor]
#define kGrayColor  [CPColor grayColor]
#define kCyanColor  [CPColor cyanColor]

#define kRandomColor [CPColor colorWithRed:((CGFloat)(arc4random()%256)/255.0) green:((CGFloat)(arc4random()%256)/255.0) blue:((CGFloat)(arc4random()%256)/255.0) alpha:((CGFloat)(arc4random()%256)/255.0)]

#define RGB(r, g, b) [CPColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:1.0f]
#define RGBA(r, g, b, a) [CPColor colorWithRed:(r/255.0f) green:(g/255.0f) blue:(b/255.0f) alpha:(a/100.0f)]

#define HexRGBColor(rgbValue) [CPColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define HexRGBAColor(rgbValue) [CPColor colorWithRed:((float)((rgbValue & 0xFF000000) >> 24))/255.0 green:((float)((rgbValue & 0xFF0000) >> 16))/255.0 blue:((float)((rgbValue & 0xFF00) >> 8))/255.0 alpha:((float)(rgbValue & 0xFF))/255.0]

#define HexARGBColor(rgbValue) [CPColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)((rgbValue & 0xFF) >> 0))/255.0 alpha:((float)(rgbValue & 0xFF) >> 24)/255.0]



