//
//  SCModel.h
//  sma11case
//
//  Created by sma11case on 15/8/23.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id(^SetValueBlock)(NSString *name, NSString *attribute, BOOL *notUpdate);

@interface SCModel : NSObject <NSCoding>
@property (nonatomic, strong, readonly) NSMutableDictionary *sc_dictionary;

+ (BOOL)sc_cachePropertyList;

+ (instancetype)sc_modelWithDictionary: (NSDictionary *)dictionary;
- (void)sc_updateWithDictionary: (NSDictionary *)dictionary;

- (NSMutableDictionary *)sc_dictionaryWithKeys: (NSArray *)keys;
- (NSMutableDictionary *)sc_dictionaryWithRecursive: (BOOL)recursive;
@end
