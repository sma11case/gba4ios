//
//  SCNetwork.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/19.
//

#import <Foundation/Foundation.h>

typedef void (^APIGenericBlock)(NSMutableURLRequest *request);
typedef void (^APIResponeBlock)(NSData *respone, NSError *error);
typedef void (^APIResponeExBlock)(NSData *respone, id state, NSError *error);

NSString *sc_mergeRequestURL(NSString *baseURL, NSDictionary *getParam);

NSData *syncBinaryRequest(NSString *url, NSDictionary *headers, NSData *postData, APIResponeBlock successBlock, APIResponeBlock failedBlock);

NSURLSessionDataTask *asyncBinaryRequest(NSString *url, NSDictionary *headers, NSData *postData, APIResponeBlock successBlock, APIResponeBlock failedBlock);

NSURLSessionDataTask *asyncBinaryRequestEx(NSString *url, NSDictionary *headers, NSData *postData, APIGenericBlock block0, APIResponeExBlock successBlock, APIResponeBlock failedBlock);

