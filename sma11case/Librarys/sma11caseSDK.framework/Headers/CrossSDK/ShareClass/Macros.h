//
//  Macros.h
//  sma11case
//
//  Created by sma11case on 15/8/11.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import "Config.h"
#import "typedef.h"

#ifndef va_list
#define va_list va_list
#endif

// 常数宏
#define MINFLOAT    (1e-999)
#define MINDOUBLE   (1e-999)
#define MAXSHORT    0x7FFF
#define MAXLONG     0x7FFFFFFFL
#define MAXUINT     ((UINT)~((UINT)0))
#define MAXULONG    ((ULONG)~((ULONG)0))
#define MAXINT      ((INT)(MAXUINT >> 1))
#define MININT      ((INT)~MAXINT)
#define PI          3.14159265358979323846

#define kDeadCode           0xDEADC0DEUL
#define kPageSize           4096UL
#define kMinMutableCount    8UL
#define kStringBufferLength 256UL
#define kIdentSpace         4

// 类型转换
#define FBridge(x, y, z) ((__bridge z)(y)x)
#define ObjectToPtr_Retain(x) ((__bridge_retained void*)x)
#define PtrToObject(type, name, ptr) type name = (__bridge type)ptr
#define PtrToObject_Release(x) ((__bridge_transfer id)(void *)x)
#define StringToURL(x)      [NSURL URLWithString:x]
#define StringToFileURL(x)  [NSURL fileURLWithPath:x];
#define NumberToString(x)   [NSString stringWithFormat:@"%@", @(x)]
#define StringToAttributedString(x) [[NSMutableAttributedString alloc] initWithString:x]

// 构造函数
#define SCConcat_(a, b) a ## b
#define SCConcat(a, b) SCConcat_(a, b)
#define SCConstructor static __attribute__((constructor)) void SCConcat(SCConstructor, __LINE__)()

// 预编译功能
#define SC_UNAVAILABLE __attribute__((unavailable))
#if IS_DEV_MODE
#define SC_DISABLED
#else
#define SC_DISABLED SC_UNAVAILABLE
#endif

// 调试宏
#if IS_DEBUG_MODE
//#define VarConstant(x) ^{return (x);}()
#define MLog(...) NSLog(__VA_ARGS__)
#define CLog(...) printf(__VA_ARGS__)
#define BreakPointHere usleep(1)
#define CrashHere [@[] objectAtIndex:1]
#define DebugSet(x, y) do{ (x) = (y); }while(false)
inline static void DebugBlock(dispatch_block_t block) {block();}
#else
//#define VarConstant(x) (x)
#define MLog(...)
#define CLog(...)
#define BreakPointHere
#define CrashHere
#define DebugSet(x, y)
#define DebugBlock(x)
#endif

// 线程宏
#define GCDBackgroundQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0)
#define GCDDefaultQueue    dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define GCDHighQueue       dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)
#define GCDMainQueue       dispatch_get_main_queue()

// 简写宏
#define IsMainThread() [NSThread isMainThread]
#define CopyAsWeak(x, y) __weak typeof(x) y = x
#define CopyAsStrong(x, y) __strong typeof(x) y = x
#define CopyAsAssign(x, y) __unsafe_unretained typeof(x) y = x
#define CopyOnly(x, y) typeof(x) y = x


#define CopyAsWeakConst(x, y) __weak const typeof(x) y = x
#define CopyAsStrongConst(x, y) __strong const typeof(x) y = x
#define CopyAsAssignConst(x, y) __unsafe_unretained const typeof(x) y = x
#define CopyOnlyConst(x, y) const typeof(x) y = x


#define IsSubclassOfClass(x, y) [x isSubclassOfClass: [y class]]
#define IsKindOfClass(x, y) [x isKindOfClass:[y class]]
#define IsMemberOfClass(x, y) [x isMemberOfClass:[y class]]
#define StringHasSubstring(x, y) ([x rangeOfString:y].length)

#define NewClass(x) [[x alloc] init]
#define NewTClass(x) ((x *)([[NSClassFromString(@(#x)) alloc] init]))

#define NewMutableString() [NSMutableString stringWithCapacity:kStringBufferLength]
#define NewMutableSet() [[NSMutableSet alloc] initWithCapacity:kMinMutableCount]
#define NewMutableArray() [NSMutableArray arrayWithCapacity:kMinMutableCount]
#define NewMutableDictionary() [NSMutableDictionary dictionaryWithCapacity:kMinMutableCount]

#define MinSleep() [NSThread sleepForTimeInterval:0.01]


// 实例宏
#define NSNC [NSNotificationCenter defaultCenter]
#define NSUD [NSUserDefaults standardUserDefaults]
#define NSUDWriteObject(x, y) do{ [NSUD setObject:y  forKey:x]; [NSUD synchronize]; } while (false)
#define NSUDWriteBool(x, y)   do{ [NSUD setBool:y    forKey:x]; [NSUD synchronize]; } while (false)
#define NSUDWriteNumber(x, y) do{ [NSUD setInteger:y forKey:x]; [NSUD synchronize]; } while (false)

// 工具宏
#define FType(x, y) ((x)y)
#define IsFTSame(type, x, y) (FType(type, x) == FType(type, y))
#define IsSameObject(x, y) IsFTSame(id, x, y)
#define IsSameString(x, y) (IsSameObject(x, y) || [x isEqualToString:y])
#define IsSameCString(x, y) ((x) == (y) || 0 == strcmp((x), (y)))

// 编码宏
#define URLEncode(x)  [(x) stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]
#define URLDecode(x)  ((x).stringByRemovingPercentEncoding)

// 实现宏
#define DefSharedMethod() + (instancetype)shared
#define ImpSharedMethod() + (instancetype)shared\
{\
    static id singleton = nil;\
    if (nil == singleton)\
    {\
        static dispatch_once_t onceToken = 0;\
        dispatch_once(&onceToken, ^{\
            singleton = [[self alloc] init];\
        });\
    }\
    return singleton;\
}

#if IS_DEBUG_MODE
#define ImpDebugDeallocMethod() - (void)dealloc\
{\
    MLog(@"%p <%@:%@> dealloc", self, [self class], self.superclass);\
}

#define ImpDebugAllocMethod() + (instancetype)alloc\
{\
    id temp = [super alloc];\
    MLog(@"%p <%@:%@> alloc", temp, [FType(NSObject *, temp) class], FType(NSObject *, temp).superclass);\
    return temp;\
}

#define ImpDebugDescriptionMethod() - (NSString *)description\
{\
NSDictionary *temp = [self propertiesWithEndClass:NSObjectClass];\
return [NSString stringWithFormat:@"<%@: %p> %@", [self class], self, (temp.count ? temp : @"")];\
}

#else
#define ImpDebugDeallocMethod()
#define ImpDebugAllocMethod()
#define ImpDebugDescriptionMethod()
#endif

