//
//  SCExceptionHandler.h
//  Demo1
//
//  Created by sma11case on 28/07/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ExceptionAliveType)
{
    ExceptionAliveTypeNone = 0,
    ExceptionAliveTypeSleep = 2,
    ExceptionAliveTypeSemaphoreWait = 4,
    ExceptionAliveTypeCopyRunLoop = 8,
};

typedef void(^SEExceptionBlock)(NSUncaughtExceptionHandler *originHandler, NSException *exception, int signal);

void enterUncaughtExceptionAction(ExceptionAliveType type);

void removeUncaughtExceptionHandler();
NSUncaughtExceptionHandler *setUncaughtExceptionHandler(SEExceptionBlock block);
