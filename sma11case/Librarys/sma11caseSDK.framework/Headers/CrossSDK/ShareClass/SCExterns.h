//
//  SCExterns.h
//  sma11case
//
//  Created by sma11case on 9/13/15.
//  Copyright (c) 2015 sma11case. All rights reserved.
//

#import "Config.h"

#define kBinaryNumberKeys  "01"
#define kOctalNumberKeys   "01234567"
#define kDecimalNumberKeys "0123456789"
#define kHexUpperCaseKeys  "0123456789ABCDEF"
#define kHexLowerCaseKeys  "0123456789abcdef"
#define kLowerCaseKeys     "abcdefghijklmnopqrstuvwxyz"
#define kUpperCaseKeys     "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define kAlphabetKeys      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define kBase64Keys        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"

#define kSCSenderKey          @"kSenderKey"
#define kSCValueKey           @"kSCValueKey"

#if RedefineSystemConstants
extern NSString *const AVCaptureSessionPresetPhoto;
extern NSString *const AVCaptureSessionPresetHigh;
extern NSString *const AVCaptureSessionPresetMedium;
extern NSString *const AVCaptureSessionPresetLow;
extern NSString *const AVCaptureSessionPreset352x288;
extern NSString *const AVCaptureSessionPreset640x480;
extern NSString *const AVCaptureSessionPreset1280x720;
extern NSString *const AVCaptureSessionPreset1920x1080;
extern NSString *const AVCaptureSessionPresetiFrame960x540;
extern NSString *const AVCaptureSessionPresetiFrame1280x720;
extern NSString *const AVCaptureSessionPresetInputPriority;
#endif

