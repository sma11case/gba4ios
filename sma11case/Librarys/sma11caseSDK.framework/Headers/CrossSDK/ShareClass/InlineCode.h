//
//  InlineCode.h
//  sma11case
//
//  Created by sma11case on 02/11/2017.
//
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <zlib.h>
#import "Macros.h"

typedef id (^WeakReference)();

inline static id safeGetIvar(id instance, const char *className, const char *varName)
{
    id obj = object_getIvar(instance, class_getInstanceVariable(objc_getClass(className), varName));
    return obj;
}

static inline SCRange SCRangeMake(Byte *location, size_t length)
{
    SCRange range;
    range.location = location;
    range.length = length;
    return range;
}

inline static double sc_abs(double a)
{
    if (a < 0) return -a;
    return a;
}

inline static void runBlockWithMain(dispatch_block_t block)
{
    dispatch_async(dispatch_get_main_queue(), block);
}

inline static void runBlockWithAsync(dispatch_block_t block)
{
    dispatch_async(GCDDefaultQueue, block);
}

inline static void runBlockAfter(NSTimeInterval time, dispatch_queue_t queue, dispatch_block_t block)
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(time * NSEC_PER_SEC)), queue, block);
}

inline static void syncBlockWithMain(dispatch_block_t block)
{
    if (NO == [NSThread isMainThread]) {
        dispatch_sync(dispatch_get_main_queue(), block);
        return;
    }
    
    block();
}

typedef BOOL(^SCSimpleTimerConditionBlock)();
inline static void sc_simpleTimer(NSTimeInterval interval, dispatch_queue_t queue, SCSimpleTimerConditionBlock condition, dispatch_block_t block)
{
    typedef void(^SCSimpleTimerBlock)(id block);
    
    SCSimpleTimerBlock b1 = ^(SCSimpleTimerBlock b2){
        runBlockAfter(interval, queue, ^{
            if (nil == condition || condition())
            {
                block();
                b2(b2);
            }
        });
    };
    b1(b1);
}

typedef BOOL(^SCSimpleTimer2ConditionBlock)(NSTimeInterval setupTime, NSTimeInterval interval, size_t count);
typedef void(^SCSimpleTimer2ExecuteBlock)(NSTimeInterval setupTime, NSTimeInterval interval, size_t count);
inline static void sc_simpleTimer2(NSTimeInterval interval, dispatch_queue_t queue, SCSimpleTimer2ConditionBlock condition, SCSimpleTimer2ExecuteBlock block)
{
    typedef void(^SCSimpleTimerBlock)(id block);
    
    __block size_t count = 0;
    const NSTimeInterval now = [NSDate date].timeIntervalSince1970;
    SCSimpleTimerBlock b1 = ^(SCSimpleTimerBlock b2){
        runBlockAfter(interval, queue, ^{
            if (nil == condition || condition(now, interval, count))
            {
                count += 1;
                block(now, interval, count);
                b2(b2);
            }
        });
    };
    b1(b1);
}

inline static void enterRunLoop(NSString *runMode)
{
    BOOL isRunning = NO;
    do
    {
        isRunning = [[NSRunLoop currentRunLoop] runMode:runMode beforeDate:[NSDate distantFuture]];
    } while (isRunning);
}

inline static WeakReference boxAsWeakReference(id object)
{
    __weak id weakref = object;
    id block = ^{ return weakref; };
    return [block copy];
}

inline static id unboxWeakReference(WeakReference ref)
{
    return (nil == ref ? nil : ref());
}

inline static NSString *getCachesDirectory()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return paths[0];
}


inline static NSString *getDocumentDirectory()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths[0];
}

inline static NSString *getTemporaryDirectory()
{
    NSString *tmpDir =  NSTemporaryDirectory();
    return tmpDir;
}

inline static NSString *getHomeDirectory()
{
    NSString *homeDir = NSHomeDirectory();
    return homeDir;
}

inline static CFIndex GetRetainCount(id x)
{
    return CFGetRetainCount((__bridge CFTypeRef)x);
}

inline static NSString *safeString(id x)
{
    if ([x isKindOfClass:[NSString class]] && [x length])
    {
        return x;
    }
    
    return @"";
}

inline static void sc_runBlockWithOnceNotify(NSString *notify, SCSenderBlock block)
{
    if (nil == block) return;
    __weak __block id s1 = [NSNC addObserverForName:notify object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [NSNC removeObserver:s1];
        block(note);
    }];
}

inline static uint32_t sc_calc_crc32(void *ptr, size_t length)
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wshorten-64-to-32"
    uLong crc = crc32(0L, Z_NULL, 0);
    crc = crc32(crc, ptr, length);
    return crc;
#pragma clang diagnostic pop
}

inline static NSRange sc_rangeAll(id obj)
{
    return NSMakeRange(0, [obj length]);
}
