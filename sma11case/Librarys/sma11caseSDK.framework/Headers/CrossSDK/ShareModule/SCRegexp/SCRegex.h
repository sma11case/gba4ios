//
//  SCRegex.h
//  sma11case
//
//  Created by sma11case on 24/04/2017.
//
//

#import <Foundation/Foundation.h>

typedef NSString*(^StringRegexMatchedBlock)(NSString *string, NSTextCheckingResult *obj, NSUInteger idx, BOOL *stop);

typedef enum {
    SCRegexNoError = 0,
    SCRegexCompileError,
    SCRegexNotMatch,
    SCRegexMatchError
} SCRegexErrorType;

struct SCRegexErrorInfo{
    SCRegexErrorType    errorType;
    int                 errorCode;
    int                 errorOffset; // maybe pattern compile error
    const char          *errorMessage;
};
typedef struct SCRegexErrorInfo SCRegexErrorInfo;

typedef BOOL(^SCRegexCallback)(SCRegexErrorInfo *errorInfo, size_t index, Byte *buffer, size_t dataSize, void *param);

extern void regexMatchFirst(void *buffer, size_t dataSize, const char *pattern, void *param, SCRegexCallback cb);

extern void regexMatch(void *buffer, size_t dataSize, const char *pattern, void *param, SCRegexCallback cb);

// string format: a.b.c.d.e(w.x.y.z), eg. 1.0.0(30)
extern NSComparisonResult compareVersionString(NSString *v1, NSString *v2);

extern NSMutableString *sc_toJsonPlainText(id json);
