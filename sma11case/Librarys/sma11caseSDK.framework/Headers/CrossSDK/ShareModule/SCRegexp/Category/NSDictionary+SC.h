//
//  NSDictionary+SC.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/31.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (sma11case_Regexp)
- (NSMutableString *)toJsonPlainText;
@end
