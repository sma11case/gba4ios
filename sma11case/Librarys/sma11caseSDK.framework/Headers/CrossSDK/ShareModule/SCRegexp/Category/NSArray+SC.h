//
//  NSArray+SC.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/31.
//

#import <Foundation/Foundation.h>

@interface NSArray (sma11case_Regexp)
- (NSMutableString *)toJsonPlainText;
@end
