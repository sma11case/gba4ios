//
//  NSData+SC.h
//  sma11case
//
//  Created by sma11case on 9/6/15.
//  Copyright (c) 2015 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../../../ShareClass/ShareClass.h"

@interface NSData(sma11case_Regexp)
- (SCRange)regexSuperCheck: (NSArray *)regexps error: (NSError **)pcreError;
- (NSData *)regexSuperMatch: (NSArray *)regexps error: (NSError **)pcreError;
@end
