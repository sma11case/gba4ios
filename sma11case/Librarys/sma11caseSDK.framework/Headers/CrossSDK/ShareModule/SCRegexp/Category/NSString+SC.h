//
//  NSString+Crypt.h
//  sma11case
//
//  Created by sma11case on 15/4/20.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../SCRegex.h"
#import "../../../ShareClass/ShareClass.h"

@interface NSString (sma11case_Regexp)
- (BOOL)isNumber;
- (BOOL)isLowercaseString;
- (BOOL)isUppercaseString;

- (BOOL)regexpCheck: (NSString *)expression;
- (NSMutableArray *)regexpMatchResults: (NSString *)expression;
- (NSMutableArray *)regexpMatch: (NSString *)expression;
- (NSString *)regexpFirstMatch: (NSString *)expression;
- (NSMutableString *)regexpReplace: (NSString *)expression replace: (NSString *)replace;
- (NSMutableString *)regexpReplace: (NSString *)expression block: (StringRegexMatchedBlock)block;

- (SCRange)regexSuperCheck: (NSArray *)regexps error: (NSError **)pcreError;
- (NSString *)regexSuperMatch: (NSArray *)regexps error: (NSError **)pcreError;
@end
