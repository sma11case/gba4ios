//
//  NSMutableString+SC.h
//  sma11case
//
//  Created by sma11case on 12/17/16.
//
//

#import <Foundation/Foundation.h>
#import "../SCRegex.h"

@interface NSMutableString (sma11case_Regexp)
- (NSUInteger)regexpMutableReplace: (NSString *)expression replace: (NSString *)replace;
- (NSUInteger)regexpMutableReplace: (NSString *)expression block: (StringRegexMatchedBlock)block;
@end
