//
//  ConsoleHook.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/21.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SEConsoleHookMode)
{
    SEConsoleHookUseBlockOnly = 0,
    SEConsoleHookKeepCLog  = (1UL << 1),
    SEConsoleHookkeepNSLog = (1UL << 2),
    SEConsoleHookKeepAll = (SEConsoleHookKeepCLog|SEConsoleHookkeepNSLog),
};

typedef void(^SEConsoleBlock)(NSString *text);

extern void sc_setConsoleHook(SEConsoleHookMode mode, SEConsoleBlock block);

