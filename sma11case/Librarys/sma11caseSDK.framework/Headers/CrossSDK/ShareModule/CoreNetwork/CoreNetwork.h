//
//  CoreNetwork.h
//  sma11case
//
//  Created by sma11case on 15/8/18.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../../ExternalsLibrary/libcurl/curl/curl.h"

#define UseLibCURL 1UL

#if UseLibCURL
typedef void(^FTPUploadBlock)(CURL *curl, CURLcode result);

NSData   *sc_urlDecode(NSString *string);
NSString *sc_urlEncode(NSString *string);
CURLcode ftp_upload(NSString *url, NSString *username, NSString *password, NSData *data, FTPUploadBlock block);
#endif
