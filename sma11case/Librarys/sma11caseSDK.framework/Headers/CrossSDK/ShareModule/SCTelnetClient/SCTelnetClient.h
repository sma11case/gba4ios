//
//  SCTelnetClient.h
//  SCTelnetClient
//
//  Created by sma11case on 18/10/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HaveSCTelnetClient 1UL
#define UseSCTelnetClient  1UL

#if UseSCTelnetClient
typedef NS_ENUM(NSUInteger, SCTelnetClientEvent)
{
    SCTelnetClientEventNone = 0,
    SCTelnetClientEventGetAddrInfoFailed,
    SCTelnetClientEventCreateSockError,
    SCTelnetClientEventBindSockError,
    SCTelnetClientEventConnectSockError,
    SCTelnetClientEventNotSupport,
    SCTelnetClientEventSendFailed,
    SCTelnetClientEventSendUnexpectedly,
    SCTelnetClientEventUnknowError,
    SCTelnetClientEventWriteStdoutError,
    SCTelnetClientEventEchoState,
    SCTelnetClientEventConnectioned,
    SCTelnetClientEventDisconnected,
    SCTelnetClientEventSettingsError,
};

@class SCTelnetClient;
typedef void(^SCTelnetClientEventBlock)(SCTelnetClient *sender, SCTelnetClientEvent event, NSDictionary *info);

@interface SCTelnetClient : NSObject
@property (nonatomic, strong) SCTelnetClientEventBlock block;

- (SCTelnetClientEvent)write:(id)data appendCRLF:(BOOL)state;
- (SCTelnetClientEvent)connect: (NSString *)host port: (ushort)port;

- (SCTelnetClientEvent)reconnect;
- (SCTelnetClientEvent)connect;
- (void)disconnect;
@end
#endif


