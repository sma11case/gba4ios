//
//  NSBundleHelper.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/23.
//

#import <Foundation/Foundation.h>
#import "../../ShareClass/ShareClass.h"
#import "NSBundleHook.h"

inline static void sc_setMainBundleInfoDictionary(NSDictionary *info)
{
    LogFunctionName();
    BreakPointHere;
    
    if (info.count)
    {
        static NSDictionary *hookedInfo = nil;
        NSMutableDictionary *dds = [kAppRealInfoDictionary mutableCopy];
        [info enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            dds[key] = obj;
        }];
        hookedInfo = [dds copy];
        sc_setInfoDictionaryHook(^id(id sender, BOOL isMainBundle, id originValue, size_t indexCount) {
            if (NO == isMainBundle) return originValue;
            return hookedInfo;
        });
        
        sc_setObjectForInfoDictionaryKeyHook(^id(id sender, BOOL isMainBundle, id originValue, size_t indexCount, id param) {
            if (NO == isMainBundle) return originValue;
            return hookedInfo[param];
        });
        return;
    }
    
    sc_setInfoDictionaryHook(nil);
    sc_setObjectForInfoDictionaryKeyHook(nil);
}

inline static void sc_setBundleIdentifier(NSString *string)
{
    NSMutableDictionary *dds = [NSMB.infoDictionary mutableCopy];
    dds[@"CFBundleIdentifier"] = safeString(string);
    sc_setMainBundleInfoDictionary(dds);
    sc_setBundleIdentifierHook(^id(id sender, BOOL isMainBundle, id originValue, size_t indexCount) {
        if (NO == isMainBundle) return originValue;
        return string;
    });
}
