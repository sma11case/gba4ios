//
//  NSBundleHook.h
//  SCPokerDylib
//
//  Created by sma11case on 23/12/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id(^SCQueryHookBlock0)(id sender, BOOL isMainBundle, id originValue, size_t indexCount);
typedef id(^SCQueryHookBlock1)(id sender, BOOL isMainBundle, id originValue, size_t indexCount, id param);

extern NSString *sc_real_bundle_id;
#ifdef DEBUG
#define kAppRealBundleId ^{return sc_real_bundle_id;}()
#else
#define kAppRealBundleId sc_real_bundle_id
#endif

extern NSDictionary *sc_real_info_dictionary;
#ifdef DEBUG
#define kAppRealInfoDictionary ^{return sc_real_info_dictionary;}()
#else
#define kAppRealInfoDictionary sc_real_info_dictionary
#endif

extern SCQueryHookBlock0 sc_bundleIdentifierBlock;
inline static void sc_setBundleIdentifierHook(SCQueryHookBlock0 block)
{
    sc_bundleIdentifierBlock = block;
}

extern SCQueryHookBlock0 sc_infoDictionaryBlock;
inline static void sc_setInfoDictionaryHook(SCQueryHookBlock0 block)
{
    sc_infoDictionaryBlock = block;
}

extern SCQueryHookBlock0 sc_localizedInfoDictionaryBlock;
inline static void sc_setLocalizedInfoDictionaryHook(SCQueryHookBlock0 block)
{
    sc_localizedInfoDictionaryBlock = block;
}

extern SCQueryHookBlock1 sc_objectForInfoDictionaryKeyBlock;
inline static void sc_setObjectForInfoDictionaryKeyHook(SCQueryHookBlock1 block)
{
    sc_objectForInfoDictionaryKeyBlock = block;
}


