//
//  GPSHook.h
//  IOSPlus
//
//  Created by sma11case on 2018/5/21.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern CLLocationCoordinate2D sc_fakeGPSCoordinate2D;
extern CLLocationCoordinate2D sc_lastGPSCoordinate2D();

inline static void setFakeGPSCoordinate(CLLocationDegrees latitude, CLLocationDegrees longitude)
{
    sc_fakeGPSCoordinate2D.latitude = latitude;
    sc_fakeGPSCoordinate2D.longitude = longitude;
}

