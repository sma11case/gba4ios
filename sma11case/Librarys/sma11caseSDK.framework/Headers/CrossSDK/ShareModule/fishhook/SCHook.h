//
//  SCHook.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/21.
//

#import <Foundation/Foundation.h>

#define sc_easy_hook(x, y) sc_hook(#x, NULL, y)
extern void *sc_hook(void *functionName, void *source, void *destination);

