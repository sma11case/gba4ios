//
//  SCUIAlertView.h
//  sma11caseA
//
//  Created by sma11case on 21/05/2018.
//

#import <UIKit/UIKit.h>

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

@interface SCUIAlertView : UIAlertView
@end

#pragma clang diagnostic pop

//#define UIAlertView SCUIAlertView

