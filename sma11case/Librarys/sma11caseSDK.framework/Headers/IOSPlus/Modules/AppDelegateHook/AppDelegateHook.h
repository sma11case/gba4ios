//
//  AppDelegateHook.h
//  IOSPlus
//
//  Created by sma11case on 20/05/2018.
//

#import <Foundation/Foundation.h>

extern NSString *const kSCUIApplicationWillRun;
extern NSString *const kDeviceTokenUpdateNotify;
extern NSString *const kSCUIApplicationDidFinishLaunchingNotification;
extern NSString *const kUIApplicationMainHooked;

extern NSData *kDeviceToken;
#ifdef DEBUG
#define kDeviceToken ^{return kDeviceToken;}()
#endif
