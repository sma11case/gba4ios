//
//  DebugUtils.h
//  IOSPlus
//
//  Created by sma11case on 2018/5/21.
//

#import <Foundation/Foundation.h>

typedef BOOL (^SEConditionBlock)(SEL sel);
typedef void (^SEBeforeBlock)(id target, SEL sel, NSArray *args, int deep);
typedef void (^SEAfterBlock)(id target, SEL sel, NSArray *args, NSTimeInterval interval, int deep, id retValue);


extern void sc_activeUIDebugingTool();

extern void se_logMethod(Class cls, SEConditionBlock condition, SEBeforeBlock beforeBlock, SEAfterBlock afterBlock);


