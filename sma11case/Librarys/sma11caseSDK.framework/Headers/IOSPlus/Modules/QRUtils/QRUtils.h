//
//  QRUtils.h
//  IOSPlus
//
//  Created by sma11case on 20/05/2018.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

typedef void(^QRScanBlock)(id sender, AVCaptureOutput *output, NSArray<__kindof AVMetadataObject *> *metadataObjects, AVCaptureConnection *connection);

@interface QRUtils : NSObject
+ (void)scanWithTypes: (NSArray *)types queue: (dispatch_queue_t)queue block: (QRScanBlock)block;
@end

@interface UIImage(QRCode)
+ (UIImage *)QRImageWithString:(NSString *)string size:(CGSize)destSize;
+ (UIImage *)QRImageWithData:(NSData *)data size:(CGSize)destSize;
@end

