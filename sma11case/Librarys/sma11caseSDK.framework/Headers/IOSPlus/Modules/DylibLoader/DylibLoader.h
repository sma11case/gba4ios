//
//  DylibLoader.h
//  IOSPlus
//
//  Created by sma11case on 2018/5/22.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, SCDylibMode)
{
    SCDylibModeNone   = 0,
    SCDylibModeLazy   = (1UL << 0),
    SCDylibModeNow    = (1UL << 1),
    SCDylibModeLocal  = (1UL << 2),
    SCDylibModeGlobal = (1UL << 3),
    
    SCDylibModeGlobalNow = (SCDylibModeNow|SCDylibModeGlobal),
};

@interface DylibLoader : NSObject
@property (nonatomic, assign, readonly) void *handle;
@property (nonatomic, strong, readonly) NSString *dylib;
@property (nonatomic, strong, readonly) NSString *lastError;
@property (nonatomic, strong, readonly) NSDictionary *symbols;

- (BOOL)open: (NSString *)dylib mode: (int)mode;
- (void *)querySymbol: (NSString *)name;
- (void)close;
@end
