//
//  SCEngine.h
//  SCEngine
//
//  Created by sma11case on 18/10/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "SEConfig.h"
#import "SEFunctions.h"
#import "Modules/SENetwork.h"
#import "Modules/SEPonyModules.h"
#import "Modules/SEEntryModule.h"
#import "Modules/SEUIWindowHook.h"
#import "../CrossSDK/ShareModule/CoreNetwork/CoreNetwork.h"

#define SEShared [SCEngine shared]
#define SELog(...) do{[SEShared.logger appendFormat:__VA_ARGS__]; [SEShared.logger appendString:@"\n"]; sc_mlog(__VA_ARGS__);}while(0)

extern NSString *const kSCEngineDidStartup;

@interface SCEngine : NSObject
@property (nonatomic, strong, readonly) NSArray *dylibs;
@property (nonatomic, strong, readonly) NSMutableString *logger;
@property (nonatomic, strong, readonly) dispatch_queue_t queue;
@property (nonatomic, assign, readonly) SEActionState weblogState;

+ (instancetype)shared;

#if (defined(SC_DISABLED) && !IS_DEV_MODE)
+ (instancetype)new SC_DISABLED;
+ (instancetype)alloc SC_DISABLED;
- (instancetype)init SC_DISABLED;
#endif

- (void)stopWebLog;
- (void)startupWebLog;

- (void)stopPonyHelper;
- (void)startupPonyHelper;

- (void)logToWeb: (NSString *)text;
- (void)ftpUpload: (id)data path: (NSString *)path queue: (dispatch_queue_t)queue block: (FTPUploadBlock)block;

- (void)ftpUploadSandboxFilesWithQueue:(dispatch_queue_t)queue;
@end

inline static void se_runBlock(dispatch_block_t block)
{
    dispatch_async(SEShared.queue, block);
}
