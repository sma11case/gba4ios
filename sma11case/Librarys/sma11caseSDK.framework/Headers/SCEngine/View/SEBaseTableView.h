//
//  SEBaseTableView.h
//  SCEngine
//
//  Created by sma11case on 13/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

inline static void removeAdjustmentBehavior(UITableView *tv)
{
    if (@available(iOS 11.0, *))
    {
        tv.estimatedRowHeight = 0;
        tv.estimatedSectionFooterHeight = 0;
        tv.estimatedSectionHeaderHeight = 0;
        tv.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
}

@interface SEBaseTableView : UITableView

@end
