//
//  SEContants.h
//  SCEngine
//
//  Created by sma11case on 21/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SEMakeStringKey(x) NSString *const x = @#x
#define SEExternStringKey(x) extern NSString *const x

#define SEMakeStaticString(x, y) static NSString *const x = y
#define SEMakeStaticStringKey(x) static NSString *const x = @#x ",builtin:" __TIMESTAMP__
#define SEMakeUnionStringKey(x) NSString *const x = @#x ",builtin:" __TIMESTAMP__

SEExternStringKey(kIPAStore);
SEExternStringKey(kAppList);
SEExternStringKey(kSEConfig);
SEExternStringKey(kSELXEModule);
SEExternStringKey(kUIDebugingTool);

SEExternStringKey(kLianLianApiUrl);
SEExternStringKey(kOwnCloudApiUrl);

