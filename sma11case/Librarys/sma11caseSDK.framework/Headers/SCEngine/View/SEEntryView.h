//
//  SEEntryView.h
//  SCEngine
//
//  Created by sma11case on 14/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEListView.h"
#import "../Modules/SEEntryModule.h"

@interface SEEntryView : SEListView
@property (nonatomic, strong) NSString *apiurl;
@property (nonatomic, strong) NSArray *menuKeys;
@end
