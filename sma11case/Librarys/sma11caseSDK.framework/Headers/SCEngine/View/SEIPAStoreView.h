//
//  SEIPAStoreView.h
//  SCEngine
//
//  Created by sma11case on 15/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEListView.h"

@interface SEIPAStoreView : SEListView
@property (nonatomic, strong) NSString *apiurl;
@end
