//
//  SEListView.h
//  SCEngine
//
//  Created by sma11case on 13/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEBaseView.h"
#import "SEBaseTableView.h"
#import "SEBaseDelegate.h"
#import "Cell/SEBaseCell.h"

typedef void(^SEPushViewBlock)(id sender, id next);

@interface SEListView : SEBaseView
@property (nonatomic, assign) CGFloat titleViewHeight;
@property (nonatomic, unsafe_unretained, readonly) UIView *titleView;

@property (nonatomic, unsafe_unretained, readonly) UIButton *leftBtn;
@property (nonatomic, unsafe_unretained, readonly) UIButton *centerBtn;
@property (nonatomic, unsafe_unretained, readonly) UIButton *rightBtn;

@property (nonatomic, strong, readonly) SEBaseDelegate *tableViewDelegate;
@property (nonatomic, unsafe_unretained, readonly) SEBaseTableView *tableView;

@property (nonatomic, weak) UIView *rootView;
@property (nonatomic, strong, readonly) SEListView *prevView;

@property (nonatomic, strong) dispatch_queue_t updateQueue;

- (void)popWithCompletion: (SEPushViewBlock)block;
- (void)push: (SEListView *)listView completion: (SEPushViewBlock)block;

- (void)reloadDataWithCompletion: (dispatch_block_t)block;
- (void)reloadTableViewWithCompletion: (dispatch_block_t)block;
@end
