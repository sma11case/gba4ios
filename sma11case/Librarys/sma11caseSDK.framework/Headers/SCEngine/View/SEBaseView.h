//
//  SEBaseView.h
//  SCEngine
//
//  Created by sma11case on 13/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SEContants.h"

typedef void(^SEViewBlock)(id sender);

@interface SEBaseView : UIView
@property (nonatomic, strong) SEViewBlock layoutSubviewsBlock;
@end
