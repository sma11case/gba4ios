//
//  SEBaseCell.h
//  SCEngine
//
//  Created by sma11case on 13/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

void se_setAttributedText(UILabel *l1, NSString *s1, NSString *s2);

typedef NS_ENUM(size_t, SECellMask)
{
    SECellMaskNone = 0,
    SECellMaskAll       = -1UL,
    SECellMaskBaseLabel = (1UL << 0),
    SECellMaskLabel     = (1UL << 1),
    SECellMaskSwitcher  = (1UL << 2),
    SECellMaskSlider    = (1UL << 3),
    SECellMaskTextField = (1UL << 4),
};

@interface SEBaseCell : UITableViewCell
@property (nonatomic, unsafe_unretained, readonly) UIView *view1;
@property (nonatomic, unsafe_unretained, readonly) UILabel *label1;
@property (nonatomic, unsafe_unretained, readonly) UISwitch *switcher;
@property (nonatomic, unsafe_unretained, readonly) UITextField *textField;
@property (nonatomic, unsafe_unretained, readonly) UISlider *slider;

@property (nonatomic, assign) size_t maskCode;
@end
