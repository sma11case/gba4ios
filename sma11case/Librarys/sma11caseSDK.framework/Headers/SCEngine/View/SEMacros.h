//
//  SEMacros.h
//  SCEngine
//
//  Created by sma11case on 14/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kSEAnimateDuration  0.3f

#define kSEBorderWidth  0.5f
#define kSEBorderHeight kSEBorderWidth

#define SESysFont(x) [UIFont systemFontOfSize:(x)]

#define kSEDescFont SESysFont(12)
#define kSEBaseFont SESysFont(14)
#define kSEMenuFont SESysFont(16)
