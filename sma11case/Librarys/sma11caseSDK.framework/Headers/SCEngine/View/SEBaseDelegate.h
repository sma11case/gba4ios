//
//  SEBaseDelegate.h
//  SCEngine
//
//  Created by sma11case on 13/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SEScrollPullBlock)(id sender, CGFloat oy);

typedef void(^SEDidSelectedCellBlock)(UITableView *sender, NSIndexPath *indexPath, id data);
typedef CGFloat(^SEHeightForRowBlock)(UITableView *sender, NSIndexPath *indexPath, id data);
typedef UITableViewCell*(^SECellForTableViewBlock)(UITableView *sender, NSIndexPath *indexPath, id data);

@interface SEBaseDelegate : UIView<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSDictionary *dataSource; //@{"0":@[], @"0":@[]}

@property (nonatomic, strong) SEDidSelectedCellBlock didSelectedCellBlock;
@property (nonatomic, strong) SEHeightForRowBlock    heightForRowBlock;
@property (nonatomic, strong) SECellForTableViewBlock cellForTableViewBlock;

@property (nonatomic, assign) CGFloat pullActionScope;
@property (nonatomic, strong) SEScrollPullBlock pullActionBlock;
@end
