//
//  SEFunctions.h
//  SCEngine
//
//  Created by sma11case on 02/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import "SEConstants.h"

// AVCaptureTorchModeOn:开 AVCaptureTorchModeOff:关
extern void sc_setFlashLightState(AVCaptureTorchMode mode);
extern AVCaptureTorchMode sc_getFlashLightState();

extern void sc_swizzleSelector(BOOL isClassMethod, id originObj, SEL originSelector, id destObj, SEL destSelector);

extern NSString *kDeviceId;
#if IS_DEBUG_MODE
#define kDeviceId ^{return kDeviceId; }()
#endif
