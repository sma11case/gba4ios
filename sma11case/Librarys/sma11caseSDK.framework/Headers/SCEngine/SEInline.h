//
//  SEInline.h
//  SCEngine
//
//  Created by sma11case on 02/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "SCEngine.h"
#import "SEFunctions.h"
#import "../CrossSDK/ShareModule/CoreNetwork/CoreNetwork.h"

#if SCReplaceLogToWebLog
#undef  NSLog
#define NSLog(...) do{  NSString *msg = [NSString stringWithFormat:__VA_ARGS__]; sc_logText(msg); }while(false)

#undef  printf
#define printf(...) do{  char *buffer = NULL; asprintf(&buffer, __VA_ARGS__); if (buffer){sc_logText(@(buffer));free(buffer);} }while(false)
#endif

inline static NSString* se_logToWeb(NSString *text)
{
    text = [NSString stringWithFormat:@"tid:%zd %@", [NSThread currentThread].seqNumber, text];
    [SEShared logToWeb:text];
    BreakPointHere;
    return text;
}

inline static void sc_logText(NSString *text)
{
    sc_printf("%s\n", text.UTF8String);
    se_logToWeb(text);
    BreakPointHere;
}

#if (SCLibVersion >= BUILD_FullVersion)
#undef LogFunctionName
#define LogFunctionName() sc_logFunctionName()
#endif

extern BOOL sc_logFunctionEnable;
inline static void sc_setLogFunction(BOOL state)
{
    sc_logFunctionEnable = state;
}

inline static void sc_logFunctionName()
{
    if (NO == sc_logFunctionEnable) return;
    
    NSString *call = [[NSThread callStackSymbols][1] substringFromIndex:40];
    sc_logText(call);
}

inline static void sc_playSystemNotifySound()
{
    SystemSoundID sound = 1007;
    AudioServicesPlaySystemSound(sound);
}

inline static void sc_ftpUpload(NSString *path, id data, FTPUploadBlock block)
{
    [SEShared ftpUpload:data path:path queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) block:block];
}

inline static CGFloat sc_getBrightness()
{
    return [UIScreen mainScreen].brightness;
}

inline static void sc_setBrightness(CGFloat x)
{
    [UIScreen mainScreen].brightness = x;
}







