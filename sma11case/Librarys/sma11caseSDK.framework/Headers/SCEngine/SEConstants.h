//
//  SEConstants.h
//  SCEngine
//
//  Created by sma11case on 21/12/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, SEActionState)
{
    SEActionStateNone = 0,
    SEActionStatePrepare = 1000,
    SEActionStateSuccess = 2000,
    SEActionStateError   = 3000,
    SEActionStatePaused  = 4000,
    SEActionStateRunning = 5000,
};

extern NSString *const kOptionSenderKey;
extern NSString *const kOptionValueKey;
