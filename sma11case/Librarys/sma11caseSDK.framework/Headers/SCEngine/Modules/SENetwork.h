//
//  SENetwork.h
//  SCEngine
//
//  Created by sma11case on 09/11/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SEProcessingEvent)
{
    SEProcessingEventNone = 0,
    SEProcessingEventWillStartLoading,
    SEProcessingEventDidStartLoading,
    SEProcessingEventWillStopLoading,
    SEProcessingEventDidStopLoading,
};

typedef void(^SEProcessingEventBlock)(NSURLRequest *request, SEProcessingEvent event, NSURLConnection *connection, BOOL *canContinue);
typedef BOOL(^SENetworkFilterBlock)(NSURLRequest *request, NSUInteger processCount);
typedef NSURLRequest *(^SENetworkModifyBlock)(NSURLRequest *request);

extern SENetworkFilterBlock     sc_filterBlock;
extern SENetworkModifyBlock     sc_modifyBlock;
extern SEProcessingEventBlock   sc_processingBlock;

extern void sc_setNetworkFilterEnable(BOOL state);

inline static void sc_setNetworkFilter(SENetworkFilterBlock block)
{
    sc_filterBlock = block;
}

inline static void sc_setNetworkModifier(SENetworkModifyBlock block)
{
    sc_modifyBlock = block;
}

inline static void sc_setNetworkProcessingHandle(SEProcessingEventBlock block)
{
    sc_processingBlock = block;
}
