//
//  SEEntryModule.h
//  SCEngine
//
//  Created by sma11case on 20/12/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../CrossSDK/ShareClass/ShareClass.h"

@interface SCEngineStartupModel : SCModel
@property (nonatomic, strong) NSString *logoURL;
@property (nonatomic, strong) NSArray *menus;
@property (nonatomic, strong) NSString *storeURL;

@property (nonatomic, assign) BOOL ignoreNetworkCheck;
@property (nonatomic, assign) BOOL justStartup;

@property (nonatomic, assign) BOOL enableWebLog;
@property (nonatomic, assign) BOOL enableLogFunction;
@property (nonatomic, assign) BOOL enableCrashSavior;

@property (nonatomic, assign) BOOL enablePonyDebugger;
@property (nonatomic, assign) BOOL ponyNetworkTrafficDebugging;
@property (nonatomic, assign) BOOL ponyForwardAllNetworkTraffic;
@property (nonatomic, strong) NSArray *ponyWatchPropertyNames;

@property (nonatomic, strong) NSDictionary *dylibSettings;

// settings for SCEngine
@property (nonatomic, assign) BOOL enableConsoleHook;
@property (nonatomic, assign) BOOL enableAutoUploadShot;

@property (nonatomic, strong) NSString *ponyHost;
@property (nonatomic, assign) ushort    ponyPort;

@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *clientName;
@property (nonatomic, strong) NSString *weblogHost;
@property (nonatomic, assign) ushort    weblogPort;

@property (nonatomic, strong) NSString *ftpUserKey;
@property (nonatomic, strong) NSString *ftpRootUrl;

@property (nonatomic, strong) NSString *lxeUserId;
@property (nonatomic, strong) NSString *lxeMobile;
@property (nonatomic, strong) NSString *lxeBuildingCode;

- (void)startup;
@end


