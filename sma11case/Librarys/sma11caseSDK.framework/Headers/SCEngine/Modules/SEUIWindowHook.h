//
//  SEUIWindowHook.h
//  SCEngine
//
//  Created by sma11case on 02/01/2018.
//  Copyright © 2018 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const kUIWindowDidMakeKeyAndVisible;
