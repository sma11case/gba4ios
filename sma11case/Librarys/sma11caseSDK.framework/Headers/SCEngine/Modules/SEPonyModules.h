//
//  SEPonyModules.h
//  SCEngine
//
//  Created by sma11case on 02/01/2018.
//  Copyright © 2018 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSEConfigKeyPonyWatchPropertyNames @"kSEConfigKeyPonyWatchPropertyNames" // bool
#define kSEConfigKeyPonyNetworkTrafficDebugging @"kSEConfigKeyPonyNetworkTrafficDebugging" // bool
#define kSEConfigKeyPonyForwardAllNetworkTraffic @"kSEConfigKeyPonyForwardAllNetworkTraffic" // bool

@class PDDebugger;
extern PDDebugger *kPDDebugger;

#ifdef DEBUG
#define kPDDebugger ^{return kPDDebugger;}()
#endif

extern void se_stopPonyHelper();
extern PDDebugger *se_startupPonyHelper(NSString *host, ushort port, NSDictionary *settings);
