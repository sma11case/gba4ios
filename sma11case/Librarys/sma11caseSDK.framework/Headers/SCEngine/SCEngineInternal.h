//
//  SCEngineInternal.h
//  sma11caseA
//
//  Created by sma11case on 2018/5/22.
//

#import "SCEngine.h"

@interface SCEngine()
@property (nonatomic, strong) NSArray *dylibs;

@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *clientName;
@property (nonatomic, strong) NSString *weblogHost;
@property (nonatomic, assign) ushort    weblogPort;

@property (nonatomic, assign) BOOL      enableConsoleHook;
@property (nonatomic, assign) BOOL      enableAutoUploadShot;

@property (nonatomic, strong) NSString *ftpUserKey;
@property (nonatomic, strong) NSString *ftpRootUrl;

@property (nonatomic, strong) NSString *ponyHost;
@property (nonatomic, assign) ushort    ponyPort;

@property (nonatomic, strong) NSString *lxeUserId;
@property (nonatomic, strong) NSString *lxeMobile;
@property (nonatomic, strong) NSString *lxeBuildingCode;

@property (nonatomic, strong) NSDictionary  *ponySettings;
@end
