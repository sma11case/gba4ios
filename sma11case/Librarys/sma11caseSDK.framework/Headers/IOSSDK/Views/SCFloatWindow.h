//
//  SCFloatWindow.h
//  sma11case
//
//  Created by sma11case on 12/9/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Config.h"

@interface SCFloatWindow : UIView
@property (nonatomic, assign) BOOL movable;

- (void)locationChange:(UIPanGestureRecognizer*)p;
@end
