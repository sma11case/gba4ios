//
//  BUIView.h
//  sample
//
//  Created by 张玺 on 12-9-10.
//  Copyright (c) 2012年 张玺. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <objc/runtime.h>

typedef void (^BlockUIEventBlock)(id sender, NSInteger buttonIndex);

@interface UIView(BUIView)<UIAlertViewDelegate,UIActionSheetDelegate>

//UIAlertView
-(void)showWithCompletionHandler:(BlockUIEventBlock)completionHandler;



//UIActionSheet
-(void)showInView:(UIView *)view withCompletionHandler:(BlockUIEventBlock)completionHandler;

-(void)showFromToolbar:(UIToolbar *)view withCompletionHandler:(BlockUIEventBlock)completionHandler;

-(void)showFromTabBar:(UITabBar *)view withCompletionHandler:(BlockUIEventBlock)completionHandler;

-(void)showFromRect:(CGRect)rect
             inView:(UIView *)view
           animated:(BOOL)animated
withCompletionHandler:(BlockUIEventBlock)completionHandler;

-(void)showFromBarButtonItem:(UIBarButtonItem *)item
                    animated:(BOOL)animated
       withCompletionHandler:(BlockUIEventBlock)completionHandler;
@end
