//
//  Macros.h
//  sma11case
//
//  Created by sma11case on 10/18/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

// 简写宏
#define NewButton() [UIButton buttonWithType:UIButtonTypeCustom]

#define GetImageWithFile(x) [[UIImage alloc] initWithContentsOfFile:x]
#define GetImageWithName(x) [UIImage imageNamed:x]

#define kAppDelegate ((id)([UIApplication sharedApplication].delegate))
#define kRootWindow [[[UIApplication sharedApplication] delegate] window]

// 设备类型判断
#define IsIPadDevice()     ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
#define IsIPhoneDevice()   ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)

