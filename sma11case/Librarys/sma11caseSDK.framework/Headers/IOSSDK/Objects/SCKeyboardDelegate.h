//
//  SCKeyboardDelegate.h
//  sma11case
//
//  Created by lianlian on 8/24/16.
//  Copyright © 2016 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SCKeyboardState)
{
    SCKeyboardStateUnknow = 0,
    SCKeyboardStateDidShow,
    SCKeyboardStateWillHide,
    SCKeyboardStateDidHide,
//    SCKeyboardStateWillShow,
//    SCKeyboardStateDidShow,
//    SCKeyboardStateWillChange,
//    SCKeyboardStateDidChange,
//    SCKeyboardStateWillHide,
//    SCKeyboardStateDidHide
};

@protocol SCKeyboardManagerDelegate <NSObject>
@optional
- (void)keyboardWillChangeFrameNotification:(NSNotification *)notify;
//- (void)keyboardWillShowNotification:(NSNotification *)notify;
- (void)keyboardWillHideNotification:(NSNotification *)notify;
- (void)keyboardDidHideNotification:(NSNotification *)notify;
@end

void closeKeyboard();

@interface SCKeyboardDelegate : NSObject
@property (nonatomic, assign, readonly) SCKeyboardState state;
@property (nonatomic, assign, readonly) CGSize size;
@property (nonatomic, assign, readonly) CGFloat durtion;
@property (nonatomic, weak, readonly) id<SCKeyboardManagerDelegate> delegate;

@property (nonatomic, assign) BOOL enable;


+ (instancetype)shared;

- (void)setKeyboardDelegate:(id <SCKeyboardManagerDelegate>)delegate;
- (void)unsetKeyboardDelegate:(id <SCKeyboardManagerDelegate>)delegate;
@end
