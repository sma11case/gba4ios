//
//  IOSUtils.h
//  sma11case
//
//  Created by sma11case on 10/15/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "../Config.h"

typedef NS_ENUM(NSUInteger, GrantType)
{
    GrantTypeReject = 0,
    GrantTypeWait,
    GrantTypeAccept,
};

extern const CGFloat kSystemTabBarHeight        ;
extern const CGFloat kSystemNavigationBarHeight ;
extern const CGFloat kSystemStatusBarHeight     ;

extern CGFloat vMinimalPT;
extern CGFloat vScreenWidth;
extern CGFloat vScreenHeight;
//extern CGFloat vStatusBarHeight;
extern double vSystemVersion;
extern NSString *kAppVersion;

#define vStatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height

#if IS_DEBUG_MODE
#define vScreenWidth     (vScreenWidth+0.0f)
#define vScreenHeight    (vScreenHeight+0.0f)
#define vSystemVersion   (vSystemVersion+0.0f)
//#define vStatusBarHeight (vStatusBarHeight+0.0f)
#define vMinimalPT       (vMinimalPT+0.0f)
#define kAppVersion      ^{return kAppVersion;}()
#endif

#define kMinimalPT       vMinimalPT
#define kScreenWidth     vScreenWidth
#define kScreenHeight    vScreenHeight
#define kSystemVersion   vSystemVersion
#define kStatusBarHeight vStatusBarHeight

GrantType isCameraValid();
GrantType isPhotoLibraryValid();

void fpsStart();
size_t getFpsCount();
void fpsStop();

NSString *getModelName(NSString *machineCode);

void sma11caseIOSInit();

CGFloat sc_getSystemVolume();
CGFloat sc_setSystemVolume(CGFloat value, BOOL showFlag, BOOL animate);
