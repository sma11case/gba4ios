//
//  IAPProxy.h
//  MiShuo
//
//  Created by sma11case on 26/04/2017.
//  Copyright © 2017 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import "../Config.h"

typedef NS_ENUM(NSUInteger, IAPState)
{
    IAPStateUnknow = 10000,
    IAPStateAppStoreInvalid,
    IAPStateFetchingProduct,
    IAPStateAddPayQueue,
    IAPStateProductNotFound,
    IAPStateFailed,
    IAPStateCanlel,
    IAPStatePurchasing,
    IAPStateDeferred,
};

typedef void(^IAPBuyStateBlock)(SKPaymentTransaction *transaction, IAPState state);
typedef void(^IAPBuySuccessBlock)(SKPaymentTransaction *transaction, NSString *receipt);
typedef BOOL(^ShouldAddStorePaymentBlock)(SKPaymentQueue *queue, SKPayment *payment, SKProduct *product);

@interface IAPProxy : NSObject
@property (nonatomic, strong, readonly) NSDictionary <NSString *, SKProduct*>*productCaches;

@property (nonatomic, strong) ShouldAddStorePaymentBlock payBlock;

- (void)updateProductListCache: (NSSet <NSString *>*)productIds block: (EmptyBlock)block;

- (BOOL)buy:(NSString *)productIdentifier success:(IAPBuySuccessBlock)block1 failed:(IAPBuyStateBlock)block2;
- (BOOL)buy: (NSString *)productIdentifier stateChange: (IAPBuyStateBlock)block0 success: (IAPBuySuccessBlock)block1 failed: (IAPBuyStateBlock)block2;
@end

