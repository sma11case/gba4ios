//
//  SCViewController.h
//  sma11case
//
//  Created by sma11case on 15/8/22.
//  Copyright (c) 2015年 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ViewControllerState)
{
    ViewControllerStateUnknow   = 0,
    ViewControllerDidLoad       = (1UL << 1),
    ViewControllerWillAppear    = (1UL << 2),
    ViewControllerDidAppear     = (1UL << 3),
    ViewControllerWillDisappear = (1UL << 4),
    ViewControllerDidDisappear  = (1UL << 5),
};

@interface SCViewController : UIViewController
@property (nonatomic, assign, readonly) BOOL isVisible;
@property (nonatomic, assign, readonly) ViewControllerState viewState;

- (void)setNavigationBarHidden: (BOOL)state;
@end
