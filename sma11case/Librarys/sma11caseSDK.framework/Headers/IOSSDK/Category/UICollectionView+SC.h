//
//  UICollectionView+SC.h
//  sma11case
//
//  Created by sma11case on 22/11/2017.
//

#import <UIKit/UIKit.h>

@interface UICollectionView(sma11case_IOS)
- (void)reloadDataWithCompletion: (dispatch_block_t)block;
@end
