//
//  UICollectionViewCell+SC.h
//  sma11case
//
//  Created by sma11case on 12/20/15.
//  Copyright © 2015 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Config.h"

@interface UICollectionViewCell(sma11case_IOS)
+ (CGSize)size;
+ (NSString *)cellId;
+ (instancetype)cellForCollectionView: (UICollectionView *)collectionView indexPath: (NSIndexPath *)indexPath;
@end
