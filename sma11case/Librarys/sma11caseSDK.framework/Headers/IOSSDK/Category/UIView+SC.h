//
//  UIView+SC.h
//  sma11case
//
//  Created by sma11case on 9/10/15.
//  Copyright (c) 2015 sma11case. All rights reserved.
//

#import <UIKit/UIKit.h>

#define GetUIViewController(name, view) __weak UIViewController *name = view.controller
#define GetViewController(type, name, view) __weak type name = (type)view.controller

typedef void(^UIEventBlock)(id sender, long event, ...);

@interface UIView(sma11case_IOS)
- (UIViewController *)controller;
- (UIImage*)saveAsImage;
- (BOOL)isVisible;

// non-public api!
- (void)_autolayoutTrace;
- (NSString *)recursiveDescription;
@end

@interface UIWindow(sma11case_IOS)
+ (UIWindow *)keyWindow;
@end
