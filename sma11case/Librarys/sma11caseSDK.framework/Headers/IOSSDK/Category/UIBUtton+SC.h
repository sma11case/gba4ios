//
//  UIBUtton+SC.h
//  IOSSDK
//
//  Created by sma11case on 19/05/2018.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIButton(sma11case_IOS)
- (void)setTitle:(NSString *)title color:(UIColor *)color state:(UIControlState)state;
@end
