//
//  UIImage+SC.h
//  sma11case
//
//  Created by sma11case on 8/29/15.
//  Copyright (c) 2015 sma11case. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "../Config.h"

typedef NS_ENUM(NSUInteger, ImageType)
{
    ImageTypeUnknow = 0,
    ImageTypeJPEG,
    ImageTypePNG,
    ImageTypeGIF,
    ImageTypeTIFF,
};

typedef void(^SaveAlbumBlock)(UIImage *image, NSError *error);

typedef NS_ENUM(NSUInteger, ScreenShotType)
{
    ScreenShotTypeUnknow = 0,
    ScreenShotTypeKeyWindow,
    ScreenShotTypeDelegateWindow,
    ScreenShotTypeAfterScreenUpdate,
};

UIImage *getScreenShot(ScreenShotType type);
ImageType getImageTypeFromData(NSData *data);

@interface UIImage(sma11case_IOS)
- (BOOL)writeToFile: (NSString *)path;
- (void)saveToAlbumWithWaitFinished: (BOOL)wait block: (SaveAlbumBlock)block;

+ (UIImage *)generateImageWithColor:(UIColor *)color;
+ (UIImage *)generateImageWithColor:(UIColor *)color size: (CGSize)size;
@end



