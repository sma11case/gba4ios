#!/bin/zsh

Root="$1"
Type="$2"

sdk=`find "${Root}" -name 'sma11caseSDK.framework' -type d | head -n 1`
dest="${sdk}/sma11caseSDK"

if [ ! -f "${dest}" ]; then
	echo "Error: sma11caseSDK.framework not found."
	echo "Usage: script.sh <sdk_search_path>"
	exit 1
fi

cd "${BUILT_PRODUCTS_DIR}"
cd "${FULL_PRODUCT_NAME}" || exit 2

tmpdir="/tmp"
newsdk="$(pwd)/Frameworks/sma11caseSDK.framework"
newdest="${newsdk}/sma11caseSDK"

crc1=`"/usr/bin/crc32" "${dest}"`
crc2=`/usr/libexec/PlistBuddy -c 'print ":SDKBinaryCRC32"' "${newdest}/Info.plist"`
[ "${crc1}" = "${crc2}" ] && exit 0

mkdir -pv "${newsdk}"
cp -f "${sdk}/Info.plist" "${newsdk}/"

rm -f "${newdest}"

if [ 'arm' = "${type}" ]; then
	lipo -thin armv7 "${dest}" -o "${tmpdir}/sma11caseSDK.armv7"
	lipo -thin arm64 "${dest}" -o "${tmpdir}/sma11caseSDK.arm64"
	lipo -create "${tmpdir}/sma11caseSDK.arm64" "${tmpdir}/sma11caseSDK.armv7" -o "${newdest}"
fi

if [ 'sim' = "${type}" ]; then
	lipo -thin armv7 "${dest}" -o "${tmpdir}/sma11caseSDK.x86_64"
	lipo -thin arm64 "${dest}" -o "${tmpdir}/sma11caseSDK.i386"
	lipo -create "${tmpdir}/sma11caseSDK.x86_64" "${tmpdir}/sma11caseSDK.i386" -o "${newdest}"
fi

"/usr/bin/codesign" --force --sign "${EXPANDED_CODE_SIGN_IDENTITY}" --timestamp=none "${newdest}"

"/usr/libexec/PlistBuddy" -c 'add ":SDKBinaryCRC32" string "${crc1}"' "${newdest}/Info.plist"

rm -f "${tmpdir}/sma11caseSDK.armv7"
rm -f "${tmpdir}/sma11caseSDK.arm64"
rm -f "${tmpdir}/sma11caseSDK.x86_64"
rm -f "${tmpdir}/sma11caseSDK.i386"

echo "sma11caseSDK update: $(date)" >> "${tmpdir}/sma11caseSDK.log"

exit
