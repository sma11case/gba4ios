GBA4iOS
===========

### 基于**GBA4IOS 2.1**修改, 增加/修改如下功能:

1. `Info.plist` 不申请任何权限
2. 修改`Debug`为不优化编译(否则有些代码不好调试)
3. Release编译时去除所有Log输出
4. 禁用**CrashlyticsFramework**, 可以设置宏`UseCrashlytics=1`恢复
5. 去除部分警告, 例如 **UIAlertView**, **UIActionSheet**, **UIPopoverController**
6. 
