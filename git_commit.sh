#!/bin/bash

SCRIPT_DIR=`dirname "$0"`
if [ '.' = "${SCRIPT_DIR:0:1}" ]; then
    SCRIPT_DIR="$(pwd)/${SCRIPT_DIR}"
fi
echo "SCRIPT_DIR = ${SCRIPT_DIR}"
cd "${SCRIPT_DIR}"

git add -A 'Cocoapods'
git add -A 'GBA4iOS'
git add -A 'SFML'
git add -A 'Skins'
git add -A 'UIImage+PDF'
git add -A 'emu-ex-plus-alpha'
git add -A 'minizip'
git add -A 'sma11case'

TODAY=`date '+%Y-%m-%d %H:%M:%S'`
if [ -n "$1" ]; then
	TODAY="$1"
fi
git commit -am "${TODAY}"

git push --all origin
git push --tags origin

exit 0

